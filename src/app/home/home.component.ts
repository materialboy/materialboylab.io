import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  id: string;

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getRoute();
  }

  getRoute(): void {
    this.activatedRoute.params.subscribe(data => {
      console.log(data);
      this.id = data.id;
    },
    (error) => {
      console.log(error);
    },
    ()=>{
      console.log("we are done with the get route");
    })
  }
}
